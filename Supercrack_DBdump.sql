CREATE DATABASE  IF NOT EXISTS `supercrack` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `supercrack`;
-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: localhost    Database: supercrack
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `administrator` (
  `uid` varchar(45) NOT NULL,
  `pw` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `usertype` int(11) DEFAULT '0' COMMENT 'Why is grade here?',
  `grade` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES ('admin','100','admin','010-0000-0000',0,0);
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `client` (
  `uid` varchar(45) NOT NULL,
  `pw` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `usertype` int(11) NOT NULL DEFAULT '2',
  `grade` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer`
--

DROP TABLE IF EXISTS `freelancer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `freelancer` (
  `uid` varchar(45) NOT NULL,
  `pw` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `usertype` varchar(45) NOT NULL DEFAULT '1',
  `grade` varchar(45) NOT NULL DEFAULT '0',
  `age` int(11) NOT NULL,
  `career` int(11) NOT NULL,
  `major` varchar(45) NOT NULL,
  `python` int(11) NOT NULL DEFAULT '0',
  `java` int(11) NOT NULL DEFAULT '0',
  `c` int(11) NOT NULL DEFAULT '0',
  `php` int(11) NOT NULL DEFAULT '0',
  `nodejs` int(11) NOT NULL DEFAULT '0',
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `PID_idx` (`pid`),
  CONSTRAINT `pid` FOREIGN KEY (`pid`) REFERENCES `participant` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer`
--

LOCK TABLES `freelancer` WRITE;
/*!40000 ALTER TABLE `freelancer` DISABLE KEYS */;
/*!40000 ALTER TABLE `freelancer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participant`
--

DROP TABLE IF EXISTS `participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `participant` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `usertype` int(11) DEFAULT NULL,
  `min_career` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participant`
--

LOCK TABLES `participant` WRITE;
/*!40000 ALTER TABLE `participant` DISABLE KEYS */;
/*!40000 ALTER TABLE `participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_external`
--

DROP TABLE IF EXISTS `portfolio_external`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `portfolio_external` (
  `uid` varchar(45) NOT NULL,
  `portid` int(11) NOT NULL COMMENT 'can be confused with participant id',
  `filename` varchar(45) NOT NULL,
  `development_info` varchar(45) NOT NULL,
  PRIMARY KEY (`uid`,`portid`),
  CONSTRAINT `externalportfolio_uid` FOREIGN KEY (`uid`) REFERENCES `freelancer` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_external`
--

LOCK TABLES `portfolio_external` WRITE;
/*!40000 ALTER TABLE `portfolio_external` DISABLE KEYS */;
/*!40000 ALTER TABLE `portfolio_external` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_internal`
--

DROP TABLE IF EXISTS `portfolio_internal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `portfolio_internal` (
  `uid` varchar(45) NOT NULL,
  `pid` int(11) NOT NULL,
  `qid` varchar(45) NOT NULL,
  `grade` int(11) NOT NULL,
  PRIMARY KEY (`uid`,`qid`),
  KEY `qid_idx` (`qid`),
  CONSTRAINT `innerportfolio_uid` FOREIGN KEY (`uid`) REFERENCES `freelancer` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_internal`
--

LOCK TABLES `portfolio_internal` WRITE;
/*!40000 ALTER TABLE `portfolio_internal` DISABLE KEYS */;
/*!40000 ALTER TABLE `portfolio_internal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest`
--

DROP TABLE IF EXISTS `quest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `quest` (
  `qid` varchar(45) NOT NULL,
  `uid` varchar(45) NOT NULL,
  `qname` varchar(45) NOT NULL,
  `qcost` int(11) NOT NULL,
  `dstartdate` date NOT NULL,
  `dfinishdate` date DEFAULT NULL,
  `min_career` int(11) NOT NULL DEFAULT '0',
  `min_freenum` int(11) NOT NULL DEFAULT '0',
  `max_freenum` int(11) DEFAULT NULL,
  `python` int(11) NOT NULL DEFAULT '0',
  `java` int(11) NOT NULL DEFAULT '0',
  `c` int(11) NOT NULL DEFAULT '0',
  `php` int(11) NOT NULL DEFAULT '0',
  `nodejs` int(11) NOT NULL DEFAULT '0',
  `pstartdate` date DEFAULT NULL,
  `pfinishdate` date DEFAULT NULL,
  PRIMARY KEY (`qid`,`uid`),
  KEY `uid_idx` (`uid`),
  CONSTRAINT `quest_uid` FOREIGN KEY (`uid`) REFERENCES `client` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest`
--

LOCK TABLES `quest` WRITE;
/*!40000 ALTER TABLE `quest` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_approval`
--

DROP TABLE IF EXISTS `quest_approval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `quest_approval` (
  `qid` varchar(45) NOT NULL,
  `pid` int(11) NOT NULL,
  `accepting` int(11) NOT NULL,
  `reject` text,
  `resultreport` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`qid`,`pid`),
  KEY `pid_idx` (`pid`),
  CONSTRAINT `quest_approval_qid` FOREIGN KEY (`qid`) REFERENCES `quest` (`qid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `quest_pid` FOREIGN KEY (`pid`) REFERENCES `participant` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_approval`
--

LOCK TABLES `quest_approval` WRITE;
/*!40000 ALTER TABLE `quest_approval` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_approval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_document`
--

DROP TABLE IF EXISTS `quest_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `quest_document` (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `qid` varchar(45) NOT NULL,
  `filename` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`did`,`qid`),
  KEY `quest_document_qid_idx` (`qid`),
  CONSTRAINT `quest_document_qid` FOREIGN KEY (`qid`) REFERENCES `quest` (`qid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_document`
--

LOCK TABLES `quest_document` WRITE;
/*!40000 ALTER TABLE `quest_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_finished`
--

DROP TABLE IF EXISTS `quest_finished`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `quest_finished` (
  `qid` varchar(45) NOT NULL,
  `clientgrade` int(11) NOT NULL,
  `participantgrade` int(11) NOT NULL,
  PRIMARY KEY (`qid`),
  CONSTRAINT `quest_finished_qid` FOREIGN KEY (`qid`) REFERENCES `quest` (`qid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_finished`
--

LOCK TABLES `quest_finished` WRITE;
/*!40000 ALTER TABLE `quest_finished` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_finished` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_participant`
--

DROP TABLE IF EXISTS `quest_participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `quest_participant` (
  `qid` varchar(45) NOT NULL,
  `pid` int(11) NOT NULL,
  `pick` int(11) NOT NULL,
  PRIMARY KEY (`qid`,`pid`),
  KEY `quest_participant_pid_idx` (`pid`),
  CONSTRAINT `quest_participant_pid` FOREIGN KEY (`pid`) REFERENCES `participant` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `quest_participant_qid` FOREIGN KEY (`qid`) REFERENCES `quest` (`qid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_participant`
--

LOCK TABLES `quest_participant` WRITE;
/*!40000 ALTER TABLE `quest_participant` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `team` (
  `tname` varchar(45) NOT NULL,
  `leader` varchar(45) NOT NULL,
  `pid` int(11) NOT NULL,
  PRIMARY KEY (`tname`),
  KEY `pid_idx` (`pid`),
  KEY `team_leader_idx` (`leader`),
  CONSTRAINT `team_leader` FOREIGN KEY (`leader`) REFERENCES `freelancer` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `team_pid` FOREIGN KEY (`pid`) REFERENCES `participant` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_member`
--

DROP TABLE IF EXISTS `team_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `team_member` (
  `tname` varchar(45) NOT NULL,
  `uid` varchar(45) NOT NULL,
  PRIMARY KEY (`uid`,`tname`),
  KEY `tname_idx` (`tname`),
  CONSTRAINT `tname` FOREIGN KEY (`tname`) REFERENCES `team` (`tname`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `uid` FOREIGN KEY (`uid`) REFERENCES `freelancer` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_member`
--

LOCK TABLES `team_member` WRITE;
/*!40000 ALTER TABLE `team_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_member` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-12 15:29:06
